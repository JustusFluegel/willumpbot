FROM debian:buster-slim

COPY out/ .
COPY package.json .
COPY package-lock.json .


SHELL ["/bin/bash", "--login", "-c"]

RUN apt-get update
RUN apt-get install -y curl

RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
RUN nvm install 14

RUN npm i

CMD ["/bin/bash", "--login", "-c", "npm run start" ]