# willumpbot 

<!-- ## Installation

1. Clone the repo
```bash
$ git clone git@gitlab.com:pixlbot/components/puppeteerpoll.git     # SSH
# OR
$ git clone https://gitlab.com/pixlbot/components/puppeteerpoll.git # HTTPS
```

2. Install dependencies
```bash
$ npm install
```

3. Compile
```bash
$ npm run tsc
```

4. Start
```bash
$ npm run start
```

## Configuration

Currently, you need to create 3 config files:

### 1 - tokens.json

The tokens.json file is the file containing all your tokens to connect to the twitch api &  login with a bot account (for polls)

To generate the tokens required, take a look at the [Twitch Authorization Documentation](https://dev.twitch.tv/docs/authentication)

It must be in the following format (every field required):
```json
{
    "clientId": "Your Twitch Application client ID",
    "clientSecret": "The secret of your Twitch application",
    "channelName": "The channel to join, the stream to listen (Username)",
    "refreshToken": "The refresh token generated for the streamer's Twitch account, using the Autorization code flow",
    "botAccountRefreshToken": "The refresh token of the account you want to use to write in the chat",
    "botAccountUsername": "The username of a twitch account used to create polls",
    "botAccountPassword": "The password for the twitch account used to create polls",
    "botEmailAccountUsername": "The username for the email account linked to the above twitch account, used for 2fa (imap)",
    "botEmailAccountPassword": "The password for the email account (imap)",
    "imapServer": "The IMAP server of the above mail account, eg. imap.google.com"
}
```

### 2 - RandomAnswers.json

Answers to choose between for internal use; 

Example (every field required) (german):

```json
{
    "blockAction": [
        "Nur weil Pixl RGB im namen hat, heißt das nicht das Farbe hier erlaubt ist! *warning* @%USER%",
        "Schön das du @%USER% dir das RGB-Konzept verinnerlicht hast! Das heißt aber noch lange nicht, dass du das auch hier im Chat wiederspiegeln musst. *warning*",
        "Farbe macht alles schöner aber leider nicht den Chat! *warning* @%USER%"
    ],
    "blockLink": [
        "Oh ein Link! Nur können Links nicht in diesem Chat erteilt werden. @%USER%",
        "Ein wildes Link erscheint! Twitch setzt Linkfilter ein! Das ist sehr effektiv! [@%USER%: Du kannst hier keine Links senden]",
        "Das U in URL steht für \"Unerlaubt\" [@%USER%: Du kannst hier keine Links senden]",
        "Das \"Hype\" im Hyperlink hat den Twitch Filter so gehyped, dass der Link rausgefiltert wurde! [@%USER%: Du kannst hier keine Links senden]"
    ],
    "invalidPollRequest": [
        "@%USER% Invalide Umfrage! Bitte benutze !poll um dir das Umfragenformat ausgeben zu lassen.",
        "@%USER% die RegEx hat mir folgendes mitgeteilt: \"YOU SHALL NOT PARSE\" [Umfrage konnte nicht automatisch bearbeitet werden, siehe !poll]"
    ],
    "manPoll": [
        "@%USER% poll man page:         <Überschrift>? 1) <Antwort 1> 2)<Antwort 2> ... 5)<Antwort 5>         Man kann maximal 5 Antwortmöglichkeiten geben",
        "@%USER% Wie erstelle ich Umfragen? Eine Umfrage kannst du erstellen indem du dir sie mit Kanalpunkten kaufst. Dies ist nur einmal pro Stream möglich. Dann gibst du deine Umfrage im Format \"<Frage> 1. <Antwort> ... 5. <Antwort>\" ein."
    ],
    "redeemedPoll": [
        "@%USER% Deine Umfrage wurde vom System erkannt und wird nun von einem Moderator bearbeitet!"
    ]
}
```

### 3 - commands.json

The file used to specify custom commands :D

Format:

```jsonc
{
    "prefix": "prefix for all commands where includePrefix isn't set to false, eg. '!'",
    "commands":{
        "syntax1": "Simple answer on !syntax1",
        "syntax2": [
            "First answer of the two to randomly choose betweeen",
            "Second answer of the two to randomly choose betweeen"
        ],
        "syntax3": {"type": "randomAnswers", "id":"reference to field in answers.json"},
        "syntax4": {
            "possibleAnswers": "Any of the above syntaxex",
            "isRegExp": false, // Default: false, allows to use regular expressions instead of a string as the trigger of the command, includePrefix is ignored.
            "regExpFlags": "g", // Flags for the regexp, ignored if isRegExp is false , default "g"
            "includePrefix": true, // Should the prefix be included in the command? default: true
            "atStart": false, // Should the command be at the beginning of the message (default: false)
            "cooldown": 10, // Cooldown between responses, default 10 seconds,
            "parameters": ["name"], // array of parameters parsed, currently not impemented (!ban name): name would be parsed
            "aliases": ["alias1", "alias2"], // array of aliases for the command, same rules as above apply here (regexp, prefix, atStart)
            "implyVariables": false, // Imply variables in the answers using a regex: default = false (Variables: %VARNAME%)
            "variables": [] // Variables used in the answers
        }
    }
}
``` -->