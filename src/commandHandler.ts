import fs from "fs";
import ChatConnection from "./chatConnection";

import { User, Channel } from "discord.js";

const DEFAULT_DEFAULT_SETTINGS: DefaultCommandSettings = {
    atStart: false,
    cooldown: 10,
    includePrefix: true,
    isRegExp: false,
    regExpFlags: "g",
};

const EXTRACT_VARIABLES_REGEXP = /(?<=%)[A-Z_]+(?=%)/g;

const MILLISECONDS_PER_SECOND = 1000;

export interface ChatCommand {
    command: string | RegExp;
    parameters: string[];
    variables: string[];
    possibleAnswers: string[];
    aliases: string[] | RegExp[];
    timeouted: boolean;
    cooldown: number;
    atStart: boolean;
}

export interface DefaultCommandSettings extends CommandSettings {
    cooldown: number;
    atStart: boolean;
    isRegExp: boolean;
    regExpFlags: string;
    includePrefix: boolean;
}
export interface CommandSettings {
    cooldown?: number;
    atStart?: boolean;
    isRegExp?: boolean;
    regExpFlags?: string;
    includePrefix?: boolean;
}

export type CommandVariable = () => PromiseLike<string> | string;
export interface CommandAnswerTypeAdvancedResult {
    solved: boolean;
    possibleAnswers?: PromiseLike<string[]> | Promise<string[]> | string[] | PromiseLike<string> | Promise<string> | string;
}
export type CommandAnswerTypeResult =
    | PromiseLike<string[]>
    | Promise<string[]>
    | string[]
    | PromiseLike<string>
    | Promise<string>
    | string
    | PromiseLike<CommandAnswerTypeAdvancedResult>
    | Promise<CommandAnswerTypeAdvancedResult>
    | CommandAnswerTypeAdvancedResult;
export type CommandAnswerType = (obj: { [key: string]: unknown; type: string }, command: string) => CommandAnswerTypeResult;

const isAdvancedResult = (obj: unknown): obj is CommandAnswerTypeAdvancedResult =>
    typeof obj === "object" &&
    (obj as { [key: string]: unknown }).solved != null &&
    typeof (obj as { [key: string]: unknown }).solved === "boolean" &&
    ((obj as { [key: string]: unknown }).possibleAnswers == null ||
        (typeof (obj as { [key: string]: unknown }).possibleAnswers === "object" && (obj as { [key: string]: unknown }).possibleAnswers instanceof Array));

export default class CommandHandler {
    chatConnection: ChatConnection;
    protected commands: ChatCommand[] = [];
    protected defaultSettings: DefaultCommandSettings;
    protected commandVariables: { [key: string]: CommandVariable } = {};
    protected commandAnswerTypes: { [key: string]: CommandAnswerType } = {};

    static fromChatConnection(chatConnection: ChatConnection, settings?: CommandSettings): CommandHandler {
        const commandHandler = new this(chatConnection, settings);
        chatConnection.onMessage(commandHandler.onMessage.bind(commandHandler));

        return commandHandler;
    }

    private constructor(chatConnection: ChatConnection, settings?: CommandSettings) {
        this.chatConnection = chatConnection;
        this.defaultSettings = {
            ...DEFAULT_DEFAULT_SETTINGS,
            ...(settings == null ? {} : settings),
        };
    }

    async addFile(filePath: string): Promise<ChatCommand[]> {
        return this.addJson(JSON.parse((await fs.promises.readFile(filePath)).toString()));
    }

    async addJson(json: {}): Promise<ChatCommand[]> {
        if (typeof (json as { [key: string]: unknown })["commands"] !== typeof {}) throw new Error("Command object doesn't exists or has an invalid structure");
        const jsonCommands: ChatCommand[] = [];

        const prefix: string | undefined = typeof (json as { [key: string]: unknown })["prefix"] === typeof "" ? ((json as { [key: string]: unknown })["prefix"] as string) : undefined;
        await Promise.all(
            Object.entries(
                (json as { [key: string]: unknown })["commands"] as {
                    [key: string]: unknown;
                },
            ).map(
                // eslint-disable-next-line @typescript-eslint/promise-function-async
                async ([key, value]) => {
                    if (this.defaultSettings.atStart == null || this.defaultSettings.cooldown == null) return;
                    const command: ChatCommand = {
                        aliases: [],
                        atStart: this.defaultSettings.atStart,
                        command: "",
                        cooldown: this.defaultSettings.cooldown,
                        parameters: [],
                        possibleAnswers: [],
                        timeouted: false,
                        variables: [],
                    };

                    let isRegexp: boolean = this.defaultSettings.isRegExp;
                    let includePrefix: boolean = this.defaultSettings.includePrefix;
                    let regExpFlags: string = this.defaultSettings.regExpFlags;

                    const answers = await this.getAnswers(value, key, false, async () => {
                        const valueobj = value as { [key: string]: unknown };

                        const possibleAnswers = (await this.getAnswers(valueobj["possibleAnswers"], key, true)) as string[];
                        isRegexp = this.extractValues<boolean>(typeof true, valueobj["isRegExp"] as boolean | undefined, this.defaultSettings.isRegExp);
                        includePrefix = this.extractValues<boolean>(typeof true, valueobj["includePrefix"] as boolean | undefined, this.defaultSettings.includePrefix);
                        command.atStart = this.extractValues<boolean>(typeof true, valueobj["atStart"] as boolean | undefined, command.atStart);
                        command.cooldown = this.extractValues<number>(typeof 1, valueobj["cooldown"] as number | undefined, command.cooldown);
                        command.parameters = this.extractValues<string[]>(typeof [""], valueobj["parameters"] as string[] | undefined, command.parameters);
                        command.variables = this.extractValues<string[]>(typeof [""], valueobj["variables"] as string[] | undefined, command.variables);
                        command.aliases = this.extractValues<string[] | RegExp[]>(typeof [""], valueobj["aliases"] as string[] | undefined, command.aliases);
                        regExpFlags = this.extractValues<string>(typeof "", valueobj["regExpFlags"] as string | undefined, this.defaultSettings.regExpFlags);

                        if (valueobj["implyVariables"]) {
                            let localVariables: string[] = [];
                            possibleAnswers.forEach((answer: string) => {
                                const matches = answer.match(EXTRACT_VARIABLES_REGEXP);
                                if (matches != null) localVariables = [...localVariables, ...matches];
                            });
                            command.variables = [...new Set([...command.variables, ...localVariables])];
                        }

                        return possibleAnswers;
                    });
                    if (!answers) {
                        throw new Error(
                            `Invalid syntax: Value of ${key} needs to be of type string, string[] or {isRegex: boolean = false, includePrefix: boolen = true, parameters?: string[], variables?: string[], possibleAnswers: string | string[], cooldown: number = 10}`,
                        );
                    }
                    command.possibleAnswers = answers as string[];

                    command.command = isRegexp ? new RegExp(key, regExpFlags) : includePrefix ? `${prefix}${key}` : key;
                    command.aliases = isRegexp
                        ? (command.aliases as string[]).map((alias: string) => new RegExp(alias, regExpFlags))
                        : includePrefix
                        ? (command.aliases as string[]).map((alias: string) => `${prefix}${alias}`)
                        : command.aliases;

                    jsonCommands.push(command);
                },
            ),
        );
        this.commands = [...this.commands, ...jsonCommands];

        return jsonCommands;
    }

    addVariable(name: string, valueCB: CommandVariable) {
        this.commandVariables[name.toUpperCase()] = valueCB;
    }

    addVariables(variables: { [key: string]: CommandVariable }) {
        Object.entries(variables).forEach(([variable, valueCB]) => {
            this.addVariable(variable, valueCB);
        });
    }

    addAnswerType(name: string, valueCB: CommandAnswerType) {
        this.commandAnswerTypes[name.toUpperCase()] = valueCB;
    }

    addAnswerTypes(variables: { [key: string]: CommandAnswerType }) {
        Object.entries(variables).forEach(([variable, valueCB]) => {
            this.addAnswerType(variable, valueCB);
        });
    }

    protected async onMessage(channel: Channel, message: string, user?: User) {
        // TODO: Add self check
        const command = this.commands.find(
            (currentCommand: ChatCommand) =>
                !currentCommand.timeouted &&
                ((currentCommand.command instanceof RegExp && currentCommand.command.exec(message) != null) ||
                    (currentCommand.atStart && message.toUpperCase().startsWith((currentCommand.command as string).toUpperCase())) ||
                    message.toUpperCase().includes((currentCommand.command as string).toUpperCase()) ||
                    currentCommand.aliases.some(
                        (alias: string | RegExp) =>
                            (alias instanceof RegExp && alias.exec(message) != null) ||
                            (currentCommand.atStart && message.toUpperCase().startsWith((alias as string).toUpperCase())) ||
                            message.toUpperCase().includes((alias as string).toUpperCase()),
                    )),
        );
        if (command != null) {
            const answer = await this.answerMessage(command, channel, message, user);
            if (answer == null) return;
            this.chatConnection.say(answer, channel);
        }
    }

    protected async answerMessage(command: ChatCommand, channel: Channel, message: string, user?: User): Promise<string | null> {
        if (command.cooldown !== 0) {
            command.timeouted = true;
            setTimeout(() => {
                command.timeouted = false;
            }, command.cooldown * MILLISECONDS_PER_SECOND);
        }

        this.addVariable("mention_user", () => `<@${user?.id}>` || "");
        this.addVariable("user_id", () => user?.id || "");
        this.addVariable("user_name", () => user?.username || "");
        this.addVariable("user_tag", () => user?.tag || "");

        const variables: { [key: string]: string } = {};
        await Promise.all(
            // eslint-disable-next-line @typescript-eslint/promise-function-async
            command.variables.map((variable: string) =>
                (async () => {
                    const valueFunction = this.commandVariables[variable.toUpperCase()];
                    if (valueFunction == null) throw new Error(`Error: Variable "${variable.toUpperCase()}" not found`);
                    variables[variable.toUpperCase()] = await valueFunction();
                })(),
            ),
        );

        if (command.possibleAnswers.length <= 0) return null;

        let choosenAnswer: string = command.possibleAnswers[Math.floor(Math.random() * command.possibleAnswers.length)];
        Object.entries(variables).forEach(([key, value]) => {
            choosenAnswer = choosenAnswer.split(`%${(key as string).toUpperCase()}%`).join(value);
        });

        return choosenAnswer;
    }

    protected async getAnswers(
        answerObj: unknown,
        command: string,
        throwIfTypeNotFound: boolean = false,
        objectCallback?: () => void | string[] | PromiseLike<void | string[]>,
    ): Promise<string[] | boolean> {
        let answers: string[] | undefined = undefined;
        if (
            answerObj == null ||
            !(
                (answerObj instanceof Array && answerObj.every((val: unknown) => typeof val === typeof "")) ||
                typeof answerObj === typeof "" ||
                (typeof answerObj === typeof {} &&
                    (objectCallback != null ||
                        (typeof answerObj === typeof {} &&
                            Object.keys(answerObj as {}).every(key => typeof key === typeof "") &&
                            typeof (answerObj as { [key: string]: unknown })["type"] === typeof "")))
            )
        )
            throw new Error(`Invalid syntax: answers of ${command} needs to be of type string or string[]`);
        if (typeof answerObj === typeof "") answers = [answerObj as string];
        else if (answerObj instanceof Array) answers = answerObj as string[];
        else if (typeof answerObj === typeof {}) {
            if (typeof (answerObj as { [key: string]: unknown })["type"] !== "string" && (answerObj as { [key: string]: unknown })["type"] != null)
                throw new Error(`type parameter must be of type string (command: ${command})`);
            if ((answerObj as { [key: string]: unknown })["type"] === undefined) (answerObj as { [key: string]: unknown })["type"] = "";
            const answerFunction = this.commandAnswerTypes[((answerObj as { [key: string]: unknown })["type"] as string).toUpperCase()];
            if (answerFunction === undefined) {
                if (objectCallback == null) throw new Error(`answers type ${(answerObj as { [key: string]: unknown })["type"]} isn't implemented (command: ${command})`);

                const cbAnswers = await objectCallback();
                if (cbAnswers != null) answers = cbAnswers;
            } else {
                const returnedAnswer = await answerFunction(answerObj as { [key: string]: unknown; type: string }, command);
                if (isAdvancedResult(returnedAnswer)) {
                    if (returnedAnswer.solved) answers = [];
                    else {
                        if (returnedAnswer.possibleAnswers == null)
                            throw new Error(`Either solved needs to be true or possibleAnswers must be defined ${JSON.stringify(returnedAnswer)} in command ${command}`);
                        const possibleAnswers = await returnedAnswer.possibleAnswers;
                        answers = typeof possibleAnswers === "object" ? possibleAnswers : [possibleAnswers];
                    }
                } else answers = typeof returnedAnswer === "object" ? returnedAnswer : [returnedAnswer];
            }
        } else if (throwIfTypeNotFound) throw new Error(`answers of ${command} needs to be of type string, string[] or {type: string, [key: string]: any}`);

        return answers == null ? false : answers;
    }

    protected extractValues<T>(type: string, value: T | undefined, defaultValue: T, extendArray = true): T {
        const val = (value != null && typeof value === type ? value : defaultValue) as T;
        if (extendArray && val instanceof Array) return ([...new Set([...(value == null ? [] : val), ...val])] as unknown) as T;

        return val;
    }
}
