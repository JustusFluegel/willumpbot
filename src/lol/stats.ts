import { CommandAnswerType } from "../commandHandler";

import { LolApi } from "twisted";
import { Regions } from "twisted/dist/constants";

export const answerHandlerId = 'lol-champ-stats'

export const answerHandler = (
  lolApi: LolApi
): CommandAnswerType =>( async (answerObj) => {
  if (answerObj.summoner == null || typeof answerObj.summoner !== "string")
    throw new Error(
      "The parameter summoner is required for the answer type 'lol-champ-stats'. ( type : string )"
    );
  if (answerObj.champ == null || typeof answerObj.champ !== "string")
    throw new Error(
      "The parameter champ is required for the answer type 'lol-champ-stats'. ( type : string )"
    );
  if (
    answerObj.region == null ||
    typeof answerObj.region !== "string" ||
    !(Object.values(Regions) as string[]).includes(answerObj.region.toUpperCase())
  )
    throw new Error(
      "The parameter region is required for the answer type 'lol-champ-stats'. ( type : Regions /* ex. EUW1 */ )"
    );
  const {
    response: { id: summonerId },
  } = await lolApi.Summoner.getByName(
    answerObj.summoner,
    answerObj.region.toUpperCase() as Regions
  );
  const champ = Object.values(
    (await lolApi.DataDragon.getChampion()).data
  ).find((champData) => champData.id == answerObj.champ);
  if (champ == undefined)
    throw new Error(`Champ ${answerObj.champ} not found! (lol-champ-stats)`);
  const champId = parseInt(champ.key);
  if (isNaN(champId)) throw new Error("Datadragon: Failded to parse key");
  const { response: stats } = await lolApi.Champion.masteryBySummonerChampion(
    summonerId,
    champId,
    answerObj.region.toUpperCase() as Regions
  );
  return `${champ.name} ist aktuell auf Mastery Level ${stats.championLevel.toString()}!\n${champ.name} braucht noch ${stats.championPointsUntilNextLevel} Punkte bis zum nächsten Level!.\n${champ.name} hat schon ${stats.championPointsSinceLastLevel} Punkte seit dem letzten Level und ${stats.tokensEarned} Tokens verdient!`;
});
