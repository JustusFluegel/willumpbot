import ChatConnection from "./chatConnection";
import { Role, TextChannel, User, Message } from "discord.js";
import { SimpleCache } from "./cache";
import fs from "fs";

const JSON_TAB_WIDTH = 4;

interface ReactionRolesConfig {
    messages: { [key: string]: { [key: string]: string } };
    channel: string;
    postedMessages: { [key: string]: string };
}

const DEFAULT_CONFIG: ReactionRolesConfig = {
    channel: "self-roles",
    messages: {},
    postedMessages: {},
};

export default class ReactionRoles {
    chatConnection?: ChatConnection;
    rolesMessages: { [key: string]: { [key: string]: Role } } = {};
    config: ReactionRolesConfig = DEFAULT_CONFIG;
    configPath?: string;
    cache?: SimpleCache;
    channel?: TextChannel;
    static async fromChatConnection(chatConnection: ChatConnection, configPath: string, cache: SimpleCache) {
        const emojiRoles = new this();
        emojiRoles.chatConnection = chatConnection;
        emojiRoles.cache = cache;
        emojiRoles.configPath = configPath;
        chatConnection?.client?.on("ready", emojiRoles._setupTypes.bind(emojiRoles));

        const notValidatedConfig = JSON.parse(await fs.promises.readFile(configPath, { encoding: "utf-8" }));

        if (notValidatedConfig.channel != null && typeof notValidatedConfig.channel !== "string")
            throw new Error(`Failed to parse ReactionRoles config file ${configPath}! Channel must be of type string.`);
        if (
            notValidatedConfig.messages == null ||
            Object.keys(notValidatedConfig.messages).some(key => typeof key !== "string") ||
            Object.values(notValidatedConfig.messages).some(
                value =>
                    value == null ||
                    typeof value !== "object" ||
                    Object.keys(value as { [key: string]: unknown }).some(key => typeof key !== "string") ||
                    Object.values(value as { [key: string]: unknown }).some(key => typeof key !== "string"),
            )
        )
            throw new Error(`Failed to parse ReactionRoles config file ${configPath}! Invalid messages object.`);
        if (
            notValidatedConfig.postedMessages != null &&
            (Object.keys(notValidatedConfig.postedMessages).some(key => typeof key !== "string") || Object.values(notValidatedConfig.postedMessages).some(key => typeof key !== "string"))
        )
            throw new Error(`Invalid postred messages Object in ${configPath}. Fix or remove it ( for resending all messages )`);

        emojiRoles.config = { ...DEFAULT_CONFIG, ...notValidatedConfig };

        return emojiRoles;
    }

    protected constructor() {}

    protected async _setupTypes() {
        const rolesArray = (await this.chatConnection?.client?.guilds.cache.first()?.roles.fetch())?.cache;
        Object.entries(this.config.messages).forEach(([message, reactionMap]) => {
            Object.entries(reactionMap).forEach(([reaction, roleName]) => {
                const role = rolesArray?.find(roleCandidate => roleCandidate.name === roleName);
                if (role == null) throw new Error(`ReactionRoles role ${roleName} not found!`);
                this.rolesMessages[message] = {};
                this.rolesMessages[message][reaction] = role;
            });
        });

        this.channel = this.chatConnection?.client?.channels.cache.find(channel => channel.type === "text" && (channel as TextChannel).name === this.config?.channel) as TextChannel | undefined;
        if (this.channel == null) throw new Error(`Failed to fetch channel to post rules! ( ${this.config?.channel} )`);

        await Promise.all(
            Object.entries(this.config.messages).map(async ([message, reactionsObject]) => {
                if (this.config?.postedMessages[message] == null) await this._postReactionRolesMessage(message, Object.keys(reactionsObject));
                else this._watchReactionRolesMessage(message, Object.keys(reactionsObject));
            }),
        );
    }

    protected async _postReactionRolesMessage(stringMessage: string, reactions: string[]) {
        const message = await this.channel?.send(stringMessage);
        if (message == null) return;
        await Promise.all(
            reactions.map(async reaction => {
                const parsedReaction = reaction.includes("<")
                    ? reaction
                          .split(">")[0]
                          .split(":")
                          ?.pop()
                    : reaction;
                if (parsedReaction == null) throw new Error(`Failed to parse reaction emoji, check your config! (${this.configPath})`);
                await message.react(parsedReaction);
            }),
        );
        this.config.postedMessages[stringMessage] = message.id;
        if (this.configPath != null) await fs.promises.writeFile(this.configPath, JSON.stringify(this.config, null, JSON_TAB_WIDTH));
        const reactionColector = message.createReactionCollector((reaction, user) => reactions.includes(reaction.emoji.toString()) && user.id !== message.author.id, { dispose: true });
        reactionColector.on("collect", (reaction, user) => this._onAddReaction(user, message, stringMessage, reaction.emoji.toString()));
        reactionColector.on("remove", (reaction, user) => this._onRemoveReaction(user, message, stringMessage, reaction.emoji.toString()));
    }

    protected async _watchReactionRolesMessage(stringMessage: string, reactions: string[]) {
        const message = this.config.postedMessages[stringMessage] == null ? null : await this.channel?.messages.fetch(this.config.postedMessages[stringMessage]);
        if (message == null) {
            this._postReactionRolesMessage(stringMessage, reactions);

            return;
        }
        await Promise.all(
            reactions.map(async reaction => {
                const usersAllowed = this.chatConnection?.client?.guilds.cache
                    .first()
                    ?.members.cache.filter(member =>
                        member.roles.cache
                            .array()
                            .map(role => role.id)
                            .includes(this.rolesMessages[stringMessage][reaction]?.id  ?? "")
                    )
                    .map(member => member.user);
                const usersReacted = (await message.reactions.cache.find(messageReaction => messageReaction.emoji.toString() === reaction)?.users.fetch())
                    ?.filter(user => user.id !== message.author.id)
                    .array();
                const usersToAllow = usersReacted?.filter(user => usersAllowed != null && !usersAllowed.map(allowedUser => allowedUser.id)?.includes(user.id));
                const usersToDisallow = usersAllowed?.filter(user => !usersReacted?.map(reactedUser => reactedUser.id).includes(user.id));
                usersToAllow?.forEach(user => this._onAddReaction(user, message, stringMessage, reaction));
                usersToDisallow?.forEach(user => this._onRemoveReaction(user, message, stringMessage, reaction));
            }),
        );
        const reactionColector = message.createReactionCollector((reaction, user) => reactions.includes(reaction.emoji.toString()) && user.id !== message.author.id, { dispose: true });
        reactionColector.on("collect", (reaction, user) => this._onAddReaction(user, message, stringMessage, reaction.emoji.toString()));
        reactionColector.on("remove", (reaction, user) => this._onRemoveReaction(user, message, stringMessage, reaction.emoji.toString()));
    }

    protected _onAddReaction(user: User, message: Message, stringMessage: string, reactionName: string) {
        const member = this.channel?.members.find(memberCandidate => memberCandidate.user.id === user.id);
        if (member == null) return;
        if (this.rolesMessages[stringMessage][reactionName] != null) member.roles.add(this.rolesMessages[message.content][reactionName]);
    }

    protected _onRemoveReaction(user: User, message: Message, stringMessage: string, reactionName: string) {
        const member = this.channel?.members.find(memberCandidate => memberCandidate.user.id === user.id);
        if (member == null) return;
        if (this.rolesMessages[stringMessage][reactionName] != null) member.roles.remove(this.rolesMessages[message.content][reactionName]);
    }
}
