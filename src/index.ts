import fs from "fs";
import RandomAnswers from "./randomAnswers";
import RegexResolvable from "./regexes";
import ChatConenction from "./chatConnection";
import CommandHandler from "./commandHandler";
import RequireAcceptRules from "./requireAcceptRules";

import { Client, ClientUser } from "discord.js";
import { JsonCache } from "./cache/jsonCache";
import ReactionRoles from "./reactionRoles";
import { NotificationHandler } from "./notificationHandler";
import { TwitchNotificationListener } from "./notificationListeners/twitch";
// import { YoutubeNotificationListener } from "./notificationListeners/youtube";
import { WebHookListener, SimpleAdapter, ReverseProxyAdapter } from "twitch-webhooks";
import { ClientCredentialsAuthProvider } from "twitch-auth";
import { ApiClient } from "twitch";

import * as champStatsAnswerHandler from "./lol/stats";

import { LolApi } from "twisted";
import { Server } from "httpanda";
import { getPortPromise } from "@d-fischer/portfinder";
import { connect } from "ngrok";
// import YoutubeNotifier from "youtube-notification";
import morgan from "morgan";

const TOKENS_CONFIG_FILE = "./configs/tokens.json";

const dev: boolean = process.env["DEVELOPMENT"] === "true";
const ngrok: boolean = process.env["USE_NGROK"] === "true" || dev;
const hostname: string = process.env["HOSTNAME"] ?? "";
const port: string = process.env["PORT"] ?? "3005";

(async () => {
    const { URL_DETECTOR } = await RegexResolvable();

    const tokensJson = {
        token: "",
        clientId: "",
        clientSecret: "",
        riotAppToken: "",
        ...JSON.parse(await fs.promises.readFile(TOKENS_CONFIG_FILE, { encoding: "utf-8" })),
    };

    const lolApi = new LolApi({ key: tokensJson.riotAppToken });

    const cache = await JsonCache.loadCache("./configs/_cache.json");

    const client = new Client();

    if (tokensJson.token === "") throw new Error(`token required (${TOKENS_CONFIG_FILE})`);
    const randomAnswers = await RandomAnswers.fromFile("./configs/answers.json");
    const chatConnection = ChatConenction.fromDiscordClient(client, undefined, randomAnswers);
    const commandHandler = CommandHandler.fromChatConnection(chatConnection);
    commandHandler.addAnswerType("randomAnswers", (answerObj, command) => {
        const { id: answerId } = answerObj;
        if (typeof answerId === typeof "" && answerId != null) return randomAnswers.getMessage(answerId as string);
        throw new Error(`The field id of the answers of ${command} for the type \"randomAnswers\" needs to be a string`);
    });
    commandHandler.addAnswerType(champStatsAnswerHandler.answerHandlerId, champStatsAnswerHandler.answerHandler(lolApi));

    await commandHandler.addFile("./configs/commands.json");
    await RequireAcceptRules.fromChatConnection(chatConnection, "./configs/rules.json");
    await ReactionRoles.fromChatConnection(chatConnection, "./configs/reactionRoles.json", cache);

    if (!hostname && !ngrok) throw new Error("Hostname is required if not using ngrok or dev mode");

    const listenerPort = ngrok? await getPortPromise(): parseInt(port, 10);
    const serverHostname = (ngrok ? await connect({ addr: listenerPort }) : hostname).replace(/^https?:\/\/|\/$/, "");

    const notificationHandler = await NotificationHandler.fromChatConnection(chatConnection, "./configs/notifications.json");
    const notificationConf = notificationHandler.unusedConfig;

    const apiClient = new ApiClient({ authProvider: new ClientCredentialsAuthProvider(tokensJson.clientId, tokensJson.clientSecret) });
    // eslint-disable-next-line require-await
    await Promise.all((await (await apiClient.helix.webHooks.getSubscriptions()).getAll()).map(async sub => sub.unsubscribe()));
    const webhookListener = new WebHookListener(
        apiClient,
        ngrok ? new ReverseProxyAdapter({ hostName: serverHostname, ssl: true, listenerPort: listenerPort, port: 443 }) : new SimpleAdapter({ listenerPort: listenerPort, hostName: serverHostname }),
        ngrok ? { hookValidity: 600 } : undefined,
    );

   //  const youtubeNotifier = new YoutubeNotifier({ hubCallback: serverHostname, middleware: true, path: "/youtube" });


    if (
        notificationConf.services != null &&
        typeof notificationConf.services === "object" &&
        Object.values(notificationConf.services as { [key: string]: unknown }).every(entry => typeof entry === "object")
    ) {
        const castedServices = notificationConf.services as { [key: string]: { [key: string]: unknown } };

        if (
            castedServices.twitch != null &&
            castedServices.twitch.users != null &&
            typeof castedServices.twitch.users === "object" &&
            Object.values(castedServices.twitch.users as { [key: string]: unknown }).every(
                user =>
                    typeof user === "object" &&
                    (user as { [key: string]: unknown }).description != null &&
                    typeof (user as { [key: string]: unknown }).description === "string" &&
                    (user as { [key: string]: unknown }).message != null &&
                    typeof (user as { [key: string]: unknown }).message === "string",
            )
        ) {
            const twitchConfig = castedServices.twitch as { users: { [key: string]: { description: string; message: string } } };
            Object.entries(twitchConfig.users).forEach(([user, info]) => {
                // eslint-disable-next-line no-console
                console.log(`Registering listener for twitch user ${user}!`);
                notificationHandler.registerListener(TwitchNotificationListener(webhookListener, apiClient, user, info.description, info.message));
            });
        }
        // if (
        //     castedServices.youtube != null &&
        //     castedServices.youtube.users != null &&
        //     typeof castedServices.youtube.users === "object" &&
        //     Object.values(castedServices.youtube.users as { [key: string]: unknown }).every(
        //         user =>
        //             typeof user === "object" &&
        //             (user as { [key: string]: unknown }).description != null &&
        //             typeof (user as { [key: string]: unknown }).description === "string" &&
        //             (user as { [key: string]: unknown }).message != null &&
        //             typeof (user as { [key: string]: unknown }).message === "string",
        //     )
        // ) {
            // const youtubeConfig = castedServices.youtube as { users: { [key: string]: { description: string; message: string } } };
            // Object.entries(youtubeConfig.users).forEach(([user, info]) => {
                // eslint-disable-next-line no-console
                // console.log(`Registering listener for youtube user (id) ${user}!`);
                // notificationHandler.registerListener(YoutubeNotificationListener(youtubeNotifier, user, info.description, info.message));
            // });
        // }
    }

    // notificationHandler.registerListener(TwitchNotificationListener(webhookListener, apiClient, "BearbyTV", "Bearby ist live!"));


    await client.login(tokensJson.token);
    await notificationHandler.listen();
    const httpandaServer = new Server();
    httpandaServer.use(morgan("combined"));
    webhookListener.applyMiddleware(httpandaServer);
    // httpandaServer.use("/youtube", youtubeNotifier.listener());
    httpandaServer.listen(listenerPort);
})();
