import ChatConnection from "./chatConnection";
import fs from "fs";
import { TextChannel, User, Message, Role } from "discord.js";

const MESSAGE_MAX_LENGTH = 2000;
const JSON_TAB_WIDTH = 4;

export interface RequireAcceptRulesConfig {
    rulesChannel: string;
    roleToAdd: string;
}

const DEFAULT_CONFIG = {
    rulesChannel: "rules",
    roleToAdd: "accepted-rules",
};

export default class RequireAcceptRules {
    protected chatConnection?: ChatConnection;
    protected rulesConfig?: { post: { id: string | null | undefined }; message: string };
    protected configPath?: string;
    protected config?: RequireAcceptRulesConfig;
    protected roleToAdd?: Role;
    protected rulesChannel?: TextChannel;
    static async fromChatConnection(chatConnection: ChatConnection, rulesConfig: string) {
        const reqAccRules = new this();
        reqAccRules.chatConnection = chatConnection;
        reqAccRules.configPath = rulesConfig;
        let notValidatedConfig: { [key: string]: unknown };
        try {
            notValidatedConfig = JSON.parse((await fs.promises.readFile(rulesConfig)).toString());
        } catch (e) {
            throw new Error(`Error whilest parsing rules config file ${rulesConfig}: ${e}`);
        }

        if (notValidatedConfig.rulesChannel != null && typeof notValidatedConfig.rulesChannel !== "string") throw new Error(`Rules channel must be string or undefined! ${rulesConfig}`);
        if (notValidatedConfig.roleToAdd != null && typeof notValidatedConfig.roleToAdd !== "string") throw new Error(`Rules role must be string or undefined! ${rulesConfig}`);

        reqAccRules.config = { ...DEFAULT_CONFIG, ...notValidatedConfig };

        if (
            notValidatedConfig.post != null &&
            (typeof notValidatedConfig.post !== "object" || (notValidatedConfig.post as { [key: string]: unknown }).id == null || typeof (notValidatedConfig.post as { [key: string]: unknown }).id) !==
                "string"
        )
            throw new Error(`invalid post entry in rules config file ${rulesConfig}, required {id: string} or null. found ${typeof notValidatedConfig.post}`);
        if (typeof notValidatedConfig.message !== "string") throw new Error(`invalid message entry in rules config file ${rulesConfig}, required string. found ${typeof notValidatedConfig.post}`);
        if (notValidatedConfig.message.length > MESSAGE_MAX_LENGTH) throw new Error(`RulesMessage has a maximum of 2000 characters, you are at ${notValidatedConfig.message.length} characters.`);
        reqAccRules.rulesConfig = {
            ...notValidatedConfig,
            post: { id: (notValidatedConfig.post as { [key: string]: unknown })?.id as string | null | undefined },
            message: notValidatedConfig.message as string,
        };

        chatConnection?.client?.on("ready", reqAccRules._setupTypes.bind(reqAccRules));

        return reqAccRules;
    }

    protected constructor() {}

    protected async _setupTypes() {
        this.roleToAdd = (await this.chatConnection?.client?.guilds.cache.first()?.roles.fetch())?.cache.find(role => role.name === this.config?.roleToAdd);
        if (this.roleToAdd == null) throw new Error(`Failed to fetch role to add on accepted rules ( ${this.config?.roleToAdd} )`);

        this.rulesChannel = this.chatConnection?.client?.channels.cache.find(channel => channel.type === "text" && (channel as TextChannel).name === this.config?.rulesChannel) as
            | TextChannel
            | undefined;
        if (this.rulesChannel == null) throw new Error(`Failed to fetch channel to post rules! ( ${this.config?.rulesChannel} )`);

        if (this.rulesConfig?.post.id == null) await this._postRules();
        else this._watchRules();
    }

    protected async _postRules() {
        const message = await this.rulesChannel?.send(this.rulesConfig?.message);
        if (message == null) return;
        await message.react("✅");
        if (this.rulesConfig != null) this.rulesConfig.post.id = message.id;
        if (this.configPath != null && this.rulesConfig != null) await fs.promises.writeFile(this.configPath, JSON.stringify(this.rulesConfig, null, JSON_TAB_WIDTH));
        const reactionColector = message.createReactionCollector((reaction, user) => reaction.emoji.name === "✅" && user.id !== message.author.id, { dispose: true });
        reactionColector.on("collect", (_, user) => this._onAddReaction(user, message));
        reactionColector.on("remove", (_, user) => this._onRemoveReaction(user, message));
    }

    protected async _watchRules() {
        const message = this.rulesConfig?.post.id == null ? null : await this.rulesChannel?.messages.fetch(this.rulesConfig?.post.id);
        if (message == null) {
            this._postRules();

            return;
        }
        const usersAllowed = this.chatConnection?.client?.guilds.cache
            .first()
            ?.members.cache.filter(member =>
                member.roles.cache
                    .array()
                    .map(role => role.id)
                    .includes(this.roleToAdd?.id ?? ""),
            )
            .map(member => member.user);
        const usersReacted = (await message.reactions.cache.find(reaction => reaction.emoji.name === "✅")?.users.fetch())?.filter(user => user.id !== message.author.id).array();
        const usersToAllow = usersReacted?.filter(user => usersAllowed != null && !usersAllowed.map(allowedUser => allowedUser.id)?.includes(user.id));
        const usersToDisallow = usersAllowed?.filter(user => !usersReacted?.map(reactedUser => reactedUser.id).includes(user.id));
        usersToAllow?.forEach(user => this._onAddReaction(user, message));
        usersToDisallow?.forEach(user => this._onRemoveReaction(user, message));
        const reactionColector = message.createReactionCollector((reaction, user) => reaction.emoji.name === "✅" && user.id !== message.author.id, { dispose: true });
        reactionColector.on("collect", (_, user) => this._onAddReaction(user, message));
        reactionColector.on("remove", (_, user) => this._onRemoveReaction(user, message));
    }

    protected _onAddReaction(user: User, message: Message) {
        const member = this.rulesChannel?.members.find(memberCandidate => memberCandidate.user.id === user.id);
        if (member == null) return;
        if (this.roleToAdd != null) member.roles.add(this.roleToAdd);
    }

    protected _onRemoveReaction(user: User, message: Message) {
        const member = this.rulesChannel?.members.find(memberCandidate => memberCandidate.user.id === user.id);
        if (member == null) return;
        if (this.roleToAdd != null) member.roles.remove(this.roleToAdd);
    }
}
