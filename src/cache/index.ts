type MaybePromise<T> = T | PromiseLike<T> | Promise<T>;

export interface SimpleCache {
    set<T>(key: string, value: T): MaybePromise<boolean>;
    get<T>(key: string): MaybePromise<T | Error>;
    has(key: string): MaybePromise<boolean>;
    isInstanceOf(key: string, probe: Function): MaybePromise<boolean>;
    isOfType(key: string, type: string): MaybePromise<boolean>;
    clear(key: string): MaybePromise<boolean>;
    clearAll(): MaybePromise<boolean>;
}
