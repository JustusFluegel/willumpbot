import { SimpleCache } from ".";
import fs from "fs";

export class JsonCache implements SimpleCache {
    data: { [key: string]: unknown } = {};
    cacheFile?: string;

    static async loadCache(cacheFile: string): Promise<JsonCache> {
        const cache = new this();
        cache.cacheFile = cacheFile;
        if (!(await cache.reload())) throw new Error(`Failed to load Cache, file: ${cacheFile}`);

        return cache;
    }

    protected constructor() {}

    async reload(): Promise<boolean> {
        if (this.cacheFile == null) return false;
        try {
            let loadedFile = {};
            if (fs.existsSync(this.cacheFile)) loadedFile = JSON.parse(await fs.promises.readFile(this.cacheFile, { encoding: "utf-8" }));
            else await this.clearAll();
            if (typeof loadedFile !== "object") await this.clearAll();
            else if (Object.keys(loadedFile).some(key => typeof key !== "string")) await this.clearAll();
            else this.data = loadedFile;
        } catch (e) {
            return false;
        }

        return true;
    }

    // eslint-disable-next-line require-await
    async set<T>(key: string, value: T): Promise<boolean> {
        this.data[key] = value;

        return this.writeToFile();
    }
    get<T>(key: string): T | Error {
        if (!this.has(key)) return new Error(`Key "${key}" not found!`);
        try {
            return this.data[key] as T;
        } catch (e) {
            return new Error(`Failed to cast to required type, Error: ${e}`);
        }
    }
    has(key: string): boolean {
        return this.data[key] != null;
    }
    isInstanceOf(key: string, probe: Function): boolean {
        return this.has(key) && this.data[key] instanceof probe;
    }

    isOfType(key: string, type: string): boolean | PromiseLike<boolean> | Promise<boolean> {
        return this.has(key) && typeof this.data[key] === type;
    }

    // eslint-disable-next-line require-await
    async clear(key: string): Promise<boolean> {
        if (!this.has(key)) return false;
        // eslint-disable-next-line fp/no-delete
        delete this.data[key];

        return this.writeToFile();
    }

    async clearAll(): Promise<boolean> {
        if (this.cacheFile == null) return false;
        try {
            await fs.promises.writeFile(this.cacheFile, "{}", { encoding: "utf-8" });

            return true;
        } catch (e) {
            return false;
        }
    }

    protected async writeToFile(): Promise<boolean> {
        if (this.cacheFile == null) return false;
        try {
            await fs.promises.writeFile(this.cacheFile, JSON.stringify(this.data, null, 0), { encoding: "utf-8" });

            return true;
        } catch (e) {
            return false;
        }
    }
}
