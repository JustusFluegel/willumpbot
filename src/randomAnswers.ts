import fs from "fs";

const isObjectWithStringKeys = (obj: unknown): obj is { [key: string]: unknown } => obj instanceof Object && Object.keys(obj).every(key => typeof key === "string");

export default class RandomAnswers {
    answers: unknown = {};
    filePath: string = "";
    variablesMap: {} = {};

    static async fromFile(filePath: string, variables?: {}): Promise<RandomAnswers> {
        const randomAnswers = new RandomAnswers(filePath);
        randomAnswers.answers = JSON.parse(await fs.promises.readFile(randomAnswers.filePath, { encoding: "utf8" }));
        if (variables != null) randomAnswers.setVariables(variables);

        return randomAnswers;
    }

    private constructor(filePath: string) {
        this.filePath = filePath;
    }

    setVariables(variables: {}) {
        if (Object.values(variables).some(v => typeof "" !== typeof v)) throw new Error("Invalid variables object format!");
        this.variablesMap = { ...this.variablesMap, ...variables };
    }

    getMessage(id: string, variables?: {}, merge = true, set = false): string {
        if (variables != null && set) this.setVariables(variables);
        if (variables != null && Object.values(variables).some(v => typeof "" !== typeof v)) throw new Error("Invalid variables object format!");
        if (this.answers == null || !isObjectWithStringKeys(this.answers)) throw new Error("");
        if (typeof this.answers[id] !== typeof [""] && typeof this.answers[id] !== typeof "") throw new Error(`Invalid answer format for id ${id}`);
        if (typeof this.answers[id] === typeof "") this.answers[id] = [this.answers[id]];
        const random = Math.floor(Math.random() * (this.answers[id] as Array<string>).length);
        const answers = this.answers[id];
        if (!isObjectWithStringKeys(answers) || answers == null || typeof answers[random] !== typeof "") throw new Error(`Invalid answer format for id ${id} at position ${random}`);
        let answer = (answers[random] as string).trim();
        if (!(variables == null && set)) {
            Object.entries(set || variables == null ? this.variablesMap : merge ? { ...this.variablesMap, ...variables } : variables).forEach(([key, value]) => {
                answer = answer.split(`%${key.toUpperCase()}%`).join(value as string);
            });
        }

        return answer;
    }
}
