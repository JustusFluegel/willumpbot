import { MessageEmbed, MessageAdditions, MessageOptions } from "discord.js";
import ChatConnection from "./chatConnection";
import fs from "fs";

interface NotificationHandlerConfig {
    channel: string;
}

const DEFAULT_CONFIG: NotificationHandlerConfig = {
    channel: "social-media-feed",
};

export class NotificationHandler {
    protected listeners: Map<(f: (message: string | MessageAdditions | MessageOptions) => void) => Promise<void> | PromiseLike<void> | void, boolean> = new Map();
    protected _listening = false;
    protected chatConnection?: ChatConnection;
    protected config: NotificationHandlerConfig = DEFAULT_CONFIG;
    protected excessConfig: {[key: string]: unknown} = {};
    protected configPath?: string;

    static async fromChatConnection(chatConnection: ChatConnection, configPath: string) {
        const notificationHandler = new this();
        notificationHandler.chatConnection = chatConnection;
        notificationHandler.configPath = configPath;
        const notValidatedConfig = JSON.parse(await fs.promises.readFile(configPath, { encoding: "utf-8" }));
        if (notValidatedConfig.channel != null && typeof notValidatedConfig.channel !== "string")
            throw new Error(`Failed to parse NotificationHandler config file ${configPath}! Channel must be of type string.`);
        notificationHandler.config = { ...DEFAULT_CONFIG, ...notValidatedConfig };
        const defaultKeys = Object.keys(DEFAULT_CONFIG);
        Object.entries(notValidatedConfig).forEach(([key, val]) => {
            if(!defaultKeys.includes(key)) notificationHandler.excessConfig[key] = val;
        });

        return notificationHandler;
    }

    get unusedConfig(){
        return this.excessConfig;
    }

    registerListener(listener: (f: (message: string | MessageAdditions | MessageOptions) => void) => Promise<void> | PromiseLike<void> | void) {
        this.listeners.set(listener, false);
    }

    removeListener(listener: (f: (message: string | MessageAdditions | MessageOptions) => void) => Promise<void> | PromiseLike<void> | void) {
        this.listeners.delete(listener);
    }

    async listen() {
        this._listening = true;
        for (const [listener, called] of this.listeners) {
            if (!called) {
                await listener(message => {
                    if (this.listening) this.chatConnection?.say(message, this.config.channel);
                });
                this.listeners.set(listener, true);
            }
        }
    }

    stop() {
        this._listening = false;
    }

    get listening() {
        return this._listening;
    }
}
