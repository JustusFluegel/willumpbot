import axios from "axios";

const STATUS_CODE_OK = 200;

export default async () => {
    const TLDListResponse = await axios.get("http://data.iana.org/TLD/tlds-alpha-by-domain.txt");
    if (TLDListResponse.status !== STATUS_CODE_OK) throw new Error(`Failed to fetch TLDs: ${TLDListResponse.statusText}`);
    const TLDArray = TLDListResponse.data
        .split("\n")
        .map((line: string) => line.trim())
        .filter((line: string) => !line.startsWith("#") && !(line === ""));

return {
        URL_DETECTOR: new RegExp(`[^\\s.]+?\\.(?:${TLDArray.join("|")})`, "gi"),
    };
};
