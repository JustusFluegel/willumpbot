import RandomAnswers from "./randomAnswers";
import { TextChannel, DMChannel, Client, Message, User, Channel, MessageAdditions, MessageOptions, NewsChannel } from "discord.js";

export default class ChatConnection {
    defaultChannel?: Channel;
    randomAnswers?: RandomAnswers;
    client?: Client;

    static fromDiscordClient(client: Client, defaultChannel?: Channel, randomAnswers?: RandomAnswers): ChatConnection {
        const chatConnection = new this();
        chatConnection.client = client;
        chatConnection.defaultChannel = defaultChannel;
        chatConnection.randomAnswers = randomAnswers;

        return chatConnection;
    }

    protected constructor() {}

    onMessage(cb: (channel: Channel, message: string, user?: User) => void) {
        this.client?.on("message", (msg: Message) => {
            if (msg.author !== this.client?.user) cb(msg.channel, msg.content, msg.author);
        });
    }

    async say(message: string | MessageAdditions | MessageOptions, channel?: string | Channel): Promise<Message | Message[] | undefined> {
        const parsedChannel =
            typeof channel === "string"
                ? this.client?.channels.cache.find(channelCandidate => (channelCandidate.type === "text" || channelCandidate.type === "news") && (channelCandidate as TextChannel | NewsChannel).name === channel)
                : channel instanceof DMChannel || channel instanceof TextChannel || channel instanceof NewsChannel
                ? channel
                : null;
        if(typeof channel === "string" && channel != null && parsedChannel == null) throw new Error(`Channel ${channel} not found!`);
        if (this.defaultChannel == null && parsedChannel == null) throw new Error("No channel specified!");
        const choosenChannel = (parsedChannel ?? this.defaultChannel) as TextChannel | DMChannel | NewsChannel;
        let sentMessage: Message | Message[] | undefined;
        if (choosenChannel != null) sentMessage = await choosenChannel.send(message);

        return sentMessage;
    }

    async sayRandomAnswer(id: string, variables?: {}, channel?: Channel, merge: boolean = true) {
        if (this.randomAnswers == null)
            throw new Error("ChatConnection.sayRandomAnswer isn't usable without an random answer handler / instance. Provide your RandomAnswer instance inside of the Constructor.");
        if (this.defaultChannel == null && channel == null) throw new Error("No channel specified!");
        await this.say(this.randomAnswers.getMessage(id, variables, merge), channel);
    }

    async deleteMessage(msg: Channel) {
        await msg.delete();
    }
}
