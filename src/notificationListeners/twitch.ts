import { MessageAdditions, MessageEmbed, MessageOptions } from "discord.js";
import { WebHookListener } from "twitch-webhooks";
import { ApiClient, HelixStream, HelixUser } from "twitch";

async function buildMessage(stream: HelixStream, user: HelixUser, description: string): Promise<MessageEmbed> {
    const game = await stream.getGame();

    return new MessageEmbed()
        .setThumbnail(user.profilePictureUrl)
        .setColor("PURPLE")
        .setAuthor(user.displayName, user.profilePictureUrl)
        .setURL(`https://twitch.tv/${user.name}`)
        .setImage(stream.thumbnailUrl.replace("{width}", "1920").replace("{height}", "1080"))
        .setTitle(stream.title)
        .setDescription(description)
        .addFields([
            { name: "Game", value: game?.name, inline: true },
            { name: "Viewers", value: stream.viewers, inline: true },
            { name: "Language", value: stream.language, inline: true },
        ]);
}

export const TwitchNotificationListener = (webhookListener: WebHookListener, twitchClient: ApiClient, channel: string, description: string, message: string = "") => async (
    f: (message: string | MessageAdditions | MessageOptions) => void,
) => {
    const helixChannel = await twitchClient.helix.users.getUserByName(channel);
    if (helixChannel == null) throw new Error(`TwitchNotificationListener: failed to get channel for username ${channel}`);
    let stream: HelixStream | null | undefined = await twitchClient.helix.streams.getStreamByUserId(helixChannel);
    if (stream != null) f(await buildMessage(stream, helixChannel, description));
    webhookListener.subscribeToStreamChanges(helixChannel, async updatedStream => {
        if (stream == null && updatedStream != null) f({ embed: await buildMessage(updatedStream, helixChannel, description), content: message });
        stream = updatedStream;
    });
};
