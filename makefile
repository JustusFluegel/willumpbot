REG_USERNAME = ${CI_REGISTRY_USER}
REG_PWD = ${CI_REGISTRY_PASSWORD}
REG_URL = ${CI_REGISTRY}

DOCKER_TAG =  ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}

default: node
	docker build -t ${DOCKER_TAG} .

node:
	npm install
	npm run tsc

publish: default
	docker login -u ${REG_USERNAME} -p ${REG_PWD} ${REG_URL}
	docker push ${DOCKER_TAG}